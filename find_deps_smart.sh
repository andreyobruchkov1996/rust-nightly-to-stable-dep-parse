#!/bin/bash
FULL_PATH=""
SUFFIX="Cargo.toml"
IS_CARGO=1

read -p 'enter wanted repository on your disk: ' REPO
#read -p 'enter wanted crate (if its main crate remain empty): ' CRATE
REPO_PATH=$(find ~ -name $REPO 2>/dev/null | head -n 1)

#if [ -z "$CRATE" ]; then CRATE="/"; fi

(cd $REPO_PATH; cargo verify-project --quiet 1>/dev/null)
IS_CARGO=$?

while [ "$IS_CARGO" -eq 1 ] 
do
    echo 'not a cargo project, try again: '
    read -p 'enter wanted repository on your disk: ' REPO
    #read -p 'enter wanted crate (if its main crate remain empty): ' CRATE
    REPO_PATH=$(find ~ -name $REPO 2>/dev/null | head -n 1)


    #if [ -z "$CRATE" ]; then CRATE="/"; fi


    (cd $REPO_PATH; cargo verify-project --quiet 1>/dev/null)
    IS_CARGO=$?
    
done

TARGET_PATH=${REPO_PATH%/*}/"$REPO"_dep_tree
TARGET_PATH_ERR_SUFFIX=$TARGET_PATH/err
TARGET_PATH_VER_ERR_SUFFIX=$TARGET_PATH/ver_err
ERR_MSG="error: could not compile"

if [ ! -d $TARGET_PATH ]; then  mkdir $TARGET_PATH; fi
if [ ! -d $TARGET_PATH_ERR_SUFFIX ]; then  mkdir $TARGET_PATH_ERR_SUFFIX; fi
if [ ! -d $TARGET_PATH_VER_ERR_SUFFIX ]; then  mkdir $TARGET_PATH_VER_ERR_SUFFIX; fi


arr=($(awk '/members/{ f = 1;next } /]/{ f=0 } {gsub(/[",]/,"",$0)}  f' $REPO_PATH/$SUFFIX ))

# delete irellevant data
for (( i=0; i < ${#arr[@]}; i++)); do
	if [[ "${arr[$i]}" == "#" ]]; then
		unset arr[i]
		unset arr[i+1]
	fi
done


LAST_DEP=""
DEP_PROBLEM=""

# execute logic on each crate
for i in "${arr[@]}"
do
	echo "running cargo test 2>$TARGET_PATH_ERR_SUFFIX/$i.err 1>/dev/null"
	(cd $REPO_PATH/$i; $(cargo test 2>$TARGET_PATH_ERR_SUFFIX/$i.err 1>/dev/null))
	echo "created err file for $REPO_PATH/$i in $TARGET_PATH_ERR_SUFFIX/$i"
	echo "running grep -F \"$ERR_MSG\" $TARGET_PATH_ERR_SUFFIX/$i.err"
	DEP_PROBLEM=$(grep -F "$ERR_MSG" $TARGET_PATH_ERR_SUFFIX/$i.err)
	if [ ! -z "$DEP_PROBLEM" ]; then
		DEP_PROBLEM=$(echo $DEP_PROBLEM | awk '{print $NF}')
		DEP_PROBLEM=$(echo "$DEP_PROBLEM" | sed -e 's/^`//' -e 's/`$//')
	else
		echo "crate $REPO_PATH/$i is ok... ."
		echo
		continue
	fi
	echo "running cargo tree -e features -i $DEP_PROBLEM"
	echo "trying to parse dependency tree for $DEP_PROBLEM in $TARGET_PATH/$i"
	(cd $REPO_PATH/$i; cargo tree -e features -i $DEP_PROBLEM 1>$TARGET_PATH/$i 2>$TARGET_PATH_VER_ERR_SUFFIX/$i)
	
	while [ $? -eq 101 ]
	do
		if [[ ! -z $LAST_DEP ]] && [[ $LAST_DEP == *"$DEP_PROBLEM"* ]]; then
			echo "trying to parse dependency tree for $LAST_DEP in  $TARGET_PATH/$i"
    			(cd $REPO_PATH/$i; cargo tree -e features -i $LAST_DEP 1>$TARGET_PATH/$i 2>/dev/null)
		else
			ver_err_msg=$(<$TARGET_PATH_VER_ERR_SUFFIX/$i)
			echo "$ver_err_msg"
			read -p 'your choise is:' LAST_DEP
			echo "running cargo tree -e features -i $LAST_DEP"
			echo "trying to parse dependency tree for $LAST_DEP in  $TARGET_PATH/$i"
			(cd $REPO_PATH/$i; cargo tree -e features -i $LAST_DEP 1>$TARGET_PATH/$i 2>/dev/null)
		fi

		echo
        done	
	echo
done 
